<h2>こちらでよろしいですか？</h2>

<?php if ($postFlag == 1) { ?>

    <table border="1">
        <tr>
            <th><label>投稿タイトル</label>：</th>
            <td><?= $postTitle ?></td>
        </tr>

        <tr>
            <th><label>アイス名</label>：</th>
            <td><?= $iceName ?></td>
        </tr>

        <tr>
            <th><label>値段帯</label>：</th>
            <td><?= $conveniencePrice_list[$price] ?></td>
        </tr>

        <tr>
            <th><label>メーカー</label>：</th>
            <td><?= $convenienceMaker_list[$maker] ?></td>
        </tr>

        <tr>
            <th><label>味</label>：</th>
            <td><?= $taste ?></td>
        </tr>

        <tr>
            <th><label>おすすめ度</label>：</th>
            <td><?= $recommend ?></td>
        </tr>

        <tr>
            <th><label>写真</label>：</th>
            <?php if (!empty($picture)) { ?>
                <td><?= $picture ?></td>
            <?php } else {  ?>
                <td><?= $picture = '無し'; ?></td>
            <?php } ?>
        </tr>

        <tr>
            <th><label>レビュー</label>：</th>
            <td><?= $review ?></td>
        </tr>
    </table>

<?php } else if ($postFlag == 2) { ?>

    <table border="1">
        <tr>
            <th><label>投稿タイトル</label>：</th>
            <td><?= $postTitle ?></td>
        </tr>
        <tr>
            <th><label>お店名</label>：</th>
            <td><?= $shopName ?></td>
        </tr>
        <tr>
            <th><label>値段帯</label>：</th>
            <td><?= $shopPrice_list[$price] ?></td>
        </tr>
        <tr>
            <th><label>所在地</label>：</th>
            <td><?= $location ?></td>
        </tr>
        <tr>
            <th><label>最寄り駅</label>：</th>
            <td><?= $station ?></td>
        </tr>
        <tr>
            <th><label>最寄り駅までの距離</label>：</th>
            <td><?= $shopDistance_list[$distance] ?></td>
        </tr>
        <tr>
            <th><label>おすすめ度</label>：</th>
            <td><?= $recommend ?></td>
        </tr>

        <tr>
            <th><label>写真</label>：</th>
            <?php if (!empty($picture)) { ?>
                <td><?= $picture ?></td>
            <?php } else {  ?>
                <td><?= $picture = '無し'; ?></td>
            <?php } ?>
        </tr>

        <tr>
            <th><label>駐車場有無</label>：</th>
            <td><?= $parkingFlg_list[$parking] ?></td>
        </tr>
        <tr>
            <th><label>レビュー</label>：</th>
            <td><?= $review ?></td>
        </tr>
    </table>

<?php } ?>

<form action="generalFlag.php" method="post">
    <input type="hidden" name="post_flag" value="<?= $postFlag ?>">

    <input type="hidden" name="post_title" value="<?= $postTitle ?>">
    <input type="hidden" name="price" value="<?= $price ?>">
    <input type="hidden" name="recommend" value="<?= $recommend ?>">
    <input type="hidden" name="picture" value="<?= $picture ?>">
    <input type="hidden" name="review" value="<?= $review ?>">

    <input type="hidden" name="ice_name" value="<?= $iceName ?>">
    <input type="hidden" name="maker" value="<?= $maker ?>">
    <input type="hidden" name="taste" value="<?= $taste ?>">

    <input type="hidden" name="shop_name" value="<?= $shopName ?>">
    <input type="hidden" name="location" value="<?= $location ?>">
    <input type="hidden" name="station" value="<?= $station ?>">
    <input type="hidden" name="distance" value="<?= $distance ?>">
    <input type="hidden" name="parking" value="<?= $parking ?>">

    <button type="submit" name="insert" value="1">投稿</button>
</form>