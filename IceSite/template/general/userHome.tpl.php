<a href="http://localhost:8888/IceSite/all/home.php">ホーム画面へ</a>
<a href="http://localhost:8888/IceSite/all/login.php">ログイン画面へ</a>
<a href="http://localhost:8888/IceSite/all/register.php">ユーザー登録画面へ</a>
<div>
    <h3>アカウント情報</h3>
    <table border="1">
        <tr>
            <th>アカウント</th>
            <th>メール</th>
        </tr>
        <?php foreach ($result as $row) { ?>
            <tr>
                <td> <?= $row['account']; ?></td>
                <td> <?= $row['mail']; ?></td>
            </tr>
        <?php } ?>
    </table>
</div>

<div>
    <h3>市販アイスの投稿</h3>
    <table border="1">
        <tr>
            <th>タイトル</th>
            <th>アイス名</th>
            <th>値段</th>
            <th>メーカー</th>
            <th>味</th>
            <th>おすすめ度</th>
            <th>写真</th>
            <th>レビュー</th>
        </tr>
        <?php foreach ($convenience as $row) { ?>
            <tr>
                <td> <?= $row['post_title']; ?></td>
                <td><?= $row['ice_name']; ?></td>
                <td><?= $conveniencePrice_list[$row['price']]; ?></td>
                <td><?= $convenienceMaker_list[$row['maker']]; ?></td>
                <td> <?= $row['taste']; ?></td>
                <td> <?= $row['recommend']; ?></td>
                <td><?= $row['picture']; ?></td>
                <td> <?= $row['review']; ?></td>
            </tr>
        <?php } ?>
    </table>
</div>

<div>
    <h3>お店の投稿</h3>
    <table border="1">
        <tr>
            <th>タイトル</th>
            <th>お店名</th>
            <th>値段帯</th>
            <th>所在地</th>
            <th>最寄り駅</th>
            <th>最寄り駅までの距離</th>
            <th>おすすめ度</th>
            <th>写真</th>
            <th>駐車場</th>
            <th>レビュー</th>
        </tr>
        <?php foreach ($shop as $row) { ?>
            <tr>
                <td> <?= $row['post_title']; ?></td>
                <td><?= $row['shop_name']; ?></td>
                <td><?= $shopPrice_list[$row['price']] ?></td>
                <td><?= $row['location']; ?></td>
                <td> <?= $row['station']; ?></td>
                <td> <?= $shopDistance_list[$row['distance']]; ?></td>
                <td> <?= $row['recommend']; ?></td>
                <td><?= $row['picture']; ?></td>
                <td> <?= $parkingFlg_list[$row['parking_flg']]; ?></td>
                <td> <?= $row['review']; ?></td>
            </tr>
        <?php } ?>
    </table>
</div>