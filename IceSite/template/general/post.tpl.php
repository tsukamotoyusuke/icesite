<form action="post.php" method="post">
    <input type="hidden" name="convinience_flag" value="1">
    <button type="submit" name="selectbtn" value="1">市販アイス</button>
</form>

<form action="post.php" method="post">
    <input type="hidden" name="shop_flag" value="1">
    <button type="submit" name="selectbtn" value="1">お店</button>
</form>

<form action="postConfirm.php" method="post">
    <?php if (!empty($convinienceFlag) or !empty($shopFlag)) { ?>
        <?php if (!empty($convinienceFlag)) { ?>
            <input type="hidden" name="post_flag" value="1">

            <table>
                <tr>
                    <th><label>投稿タイトル</label>：</th>
                    <td><input type="text" name="post_title"></td>
                </tr>
                <tr>
                    <th><label>アイス名</label>：</th>
                    <td><input type="text" name="ice_name"></td>
                </tr>
                <tr>
                    <th><label>値段帯</label>：</th>
                    <td>
                        <select name="price">
                            <?php foreach ($conveniencePrice_list as $key => $row) { ?>
                                <option value="<?= $key ?>"><?= $row ?></option>
                            <?php } ?>
                        </select>
                    </td>
                </tr>
                <tr>
                    <th><label>メーカー</label>：</th>
                    <td>
                        <select name="maker">
                            <?php foreach ($convenienceMaker_list as $key => $row) { ?>
                                <option value="<?= $key ?>"><?= $row ?></option>
                            <?php } ?>
                        </select>
                    </td>
                </tr>
                <tr>
                    <th><label>味</label>：</th>
                    <td><input type="text" name="taste"></td>
                </tr>
                <tr>
                    <th><label>おすすめ度</label>：</th>
                    <td>
                        <select name="recommend">
                            <?php for ($i = 1; $i <= 5; $i++) { ?>
                                <option value="<?= $i ?>"><?= $i ?></option>
                            <?php } ?>
                        </select>
                    </td>
                </tr>
                <tr>
                    <th><label>写真</label>：</th>
                    <td><input type="text" name="picture"></td>
                </tr>
                <tr>
                    <th><label>レビュー</label>：</th>
                    <td><textarea name="review"></textarea></td>
                </tr>
            </table>
        <?php } else if (!empty($shopFlag)) { ?>
            <input type="hidden" name="post_flag" value="2">

            <table>
                <tr>
                    <th><label>投稿タイトル</label>：</th>
                    <td><input type="text" name="post_title"></td>
                </tr>
                <tr>
                    <th><label>お店名</label>：</th>
                    <td><input type="text" name="shop_name"></td>
                </tr>
                <tr>
                    <th><label>値段帯</label>：</th>
                    <td>
                        <select name="price">
                            <?php foreach ($shopPrice_list as $key => $row) { ?>
                                <option value="<?= $key ?>"><?= $row ?></option>
                            <?php } ?>
                        </select>
                    </td>
                </tr>
                <tr>
                    <th><label>所在地</label>：</th>
                    <td><input type="text" name="location"></td>
                </tr>
                <tr>
                    <th><label>最寄り駅</label>：</th>
                    <td><input type="text" name="station"></td>
                </tr>
                <tr>
                    <th><label>最寄り駅までの距離</label>：</th>
                    <td>
                        <select name="distance">
                            <?php foreach ($shopDistance_list as $key => $row) { ?>
                                <option value="<?= $key ?>"><?= $row ?></option>
                            <?php } ?>
                        </select>
                    </td>
                </tr>
                <tr>
                    <th><label>おすすめ度</label>：</th>
                    <td>
                        <select name="recommend">
                            <?php for ($i = 1; $i <= 5; $i++) { ?>
                                <option value="<?= $i ?>"><?= $i ?></option>
                            <?php } ?>
                        </select>
                    </td>
                </tr>
                <tr>
                    <th><label>写真</label>：</th>
                    <td><input type="text" name="picture"></td>
                </tr>
                <tr>
                    <th><label>駐車場有無</label>：</th>
                    <td>
                        <select name="parking">
                            <?php foreach ($parkingFlg_list as $key => $row) { ?>
                                <option value="<?= $key ?>"><?= $row ?></option>
                            <?php } ?>
                        </select>
                    </td>
                </tr>
                <tr>
                    <th><label>レビュー</label>：</th>
                    <td>
                        <textarea name="review"></textarea>
                    </td>
                </tr>
            </table>
        <?php } ?>

        <button type="submit" name="insert" value="1">投稿</button>
    <?php } ?>
</form>