<?php



//市販アイス投稿取得
function getConvenienceIce($dbh)
{
    $sql = 'SELECT ci.picture , ci.ice_name , ci.price , ci.taste , ci.post_title , ci.recommend , ci.review , ci.maker , iu.account
            FROM convenience_ice as ci inner join ice_user as iu where iu.id = ci.user_id';
    $stmt = $dbh->prepare($sql);
    $stmt->execute();
    return $stmt->fetchAll();
}

//お店投稿取得
function getShopIce($dbh)
{
    $sql = 'SELECT si.shop_name , si.price , si.location , si.post_title , si.recommend , si.review , si.station , si.distance , si.picture , si.parking_flg , iu.account
            FROM shop_ice as si inner join ice_user as iu where iu.id = si.user_id';
    $stmt = $dbh->prepare($sql);
    $stmt->execute();
    return $stmt->fetchAll();
}

//ユーザー登録
function iceUserInsert($dbh, $id, $account, $hpass,  $mail, $userFlag)
{
    $sql = "INSERT INTO ice_user ( id , account , password , mail , created_at , updated_at , user_flag) 
                    values( :id , :account , :password ,  :mail , :created_at , :updated_at , :user_flag)";
    $stmt = $dbh->prepare($sql);
    $stmt->bindValue('id', $id);
    $stmt->bindValue('account', $account);
    $stmt->bindValue('password', $hpass);
    $stmt->bindValue('mail', $mail);
    $stmt->bindValue('user_flag', $userFlag);
    $stmt->bindValue('created_at', date('Y-m-d H:i:s'));
    $stmt->bindValue('updated_at', date('Y-m-d H:i:s'));
    $stmt->execute();

    return $stmt;
}

//ログイン
function generalLogin($dbh, $id, $password)
{
    $sql = "SELECT id , account , password , user_flag from ice_user where id = :id ";
    $stmt = $dbh->prepare($sql);
    $stmt->bindValue('id', $id);
    $stmt->execute();

    $checkPass = $stmt->fetch(PDO::FETCH_ASSOC);
    //パスワードチェック
    if (!password_verify($password, $checkPass['password'])) {
        $result = false;
    } else {
        $result = array(true, $checkPass['account'], $checkPass['user_flag']);
    }

    return $result;
}


//市販アイス投稿
function convenienceIceInsert($dbh, $postTitle, $iceName, $price, $maker, $taste, $recommend, $picture, $review)
{
    $sql = "INSERT INTO convenience_ice ( user_id , ice_name , price , maker , taste , recommend , post_title , picture , review ,created_at , updated_at) 
    values ( :user_id , :ice_name , :price , :maker , :taste , :recommend , :post_title , :picture , :review , :created_at , :updated_at) ";
    $stmt = $dbh->prepare($sql);
    $stmt->bindValue('user_id', $_SESSION["id"]);
    $stmt->bindValue('ice_name', $iceName);
    $stmt->bindValue('price', $price);
    $stmt->bindValue('maker', $maker);
    $stmt->bindValue('taste', $taste);
    $stmt->bindValue('recommend', $recommend);
    $stmt->bindValue('post_title', $postTitle);
    $stmt->bindValue('picture', $picture);
    $stmt->bindValue('review', $review);
    $stmt->bindValue('created_at', date('Y-m-d H:i:s'));
    $stmt->bindValue('updated_at', date('Y-m-d H:i:s'));
    $stmt->execute();

    return $stmt;
}

//お店投稿
function shopIceInsert($dbh, $postTitle, $shopName, $price, $location, $station, $distance, $recommend, $picture, $parking, $review)
{
    $sql = "INSERT INTO shop_ice ( user_id , post_title , shop_name , price , location , station , distance , recommend , picture , parking_flg , review ,created_at , updated_at) 
    values ( :user_id , :post_title , :shop_name , :price , :location ,:station , :distance , :recommend , :picture , :parking_flg , :review , :created_at , :updated_at) ";
    $stmt = $dbh->prepare($sql);
    $stmt->bindValue('user_id', $_SESSION["id"]);
    $stmt->bindValue('post_title', $postTitle);
    $stmt->bindValue('shop_name', $shopName);
    $stmt->bindValue('price', $price);
    $stmt->bindValue('location', $location);
    $stmt->bindValue('station', $station);
    $stmt->bindValue('distance', $distance);
    $stmt->bindValue('recommend', $recommend);
    $stmt->bindValue('picture', $picture);
    $stmt->bindValue('parking_flg', $parking);
    $stmt->bindValue('review', $review);
    $stmt->bindValue('created_at', date('Y-m-d H:i:s'));
    $stmt->bindValue('updated_at', date('Y-m-d H:i:s'));
    $stmt->execute();

    return $stmt;
}

//ユーザー情報取得
function getIceUser($dbh)
{
    $sql = "SELECT * from ice_user where id = :id ";
    $stmt = $dbh->prepare($sql);
    $stmt->bindValue('id', $_SESSION["id"]);
    $stmt->execute();

    return $stmt;
}

//ユーザー市販アイス投稿取得
function getUserConveniencePost($dbh)
{
    $sql = "SELECT * from convenience_ice where user_id = :id ";
    $stmt = $dbh->prepare($sql);
    $stmt->bindValue('id', $_SESSION["id"]);
    $stmt->execute();

    return $stmt;
}

//ユーザーお店投稿取得
function getUserShopPost($dbh)
{
    $sql = "SELECT * from shop_ice where user_id = :id ";
    $stmt = $dbh->prepare($sql);
    $stmt->bindValue('id', $_SESSION["id"]);
    $stmt->execute();

    return $stmt;
}
