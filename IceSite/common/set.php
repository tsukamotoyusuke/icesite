<?php
$dbName = 'mysql';
$host = 'localhost:8889';
$dsn = "mysql:host={$host};dbname={$dbName};charset=utf8";

$user = 'root';
$password = 'root';

try {
    $dbh = new PDO($dsn, $user, $password);
} catch (PDOException $e) {
    echo 'データにアクセスできません' . $e->getMessage();
    exit;
}

session_start();

date_default_timezone_set('Asia/Tokyo');
