<!DOCTYPE html>
<html lang="ja">

<head>
    <meta charset="utf-8">
    <title><?= $title ?></title>
    <link href="../../css/style.css" rel="stylesheet">
</head>

<body>
    <h1><?= $title ?></h1>
    <?php if (!is_null($_SESSION["id"])) { ?>
        <?php if ($_SESSION["user_flag"] == 0) { ?>
            <a href="http://localhost:8888/IceSite/general/post.php">投稿画面へ</a>
            <a href="http://localhost:8888/IceSite/all/logout.php">ログアウト画面へ</a>
            <a href="http://localhost:8888/IceSite/general/userHome.php">ユーザー画面へ</a>
        <?php } elseif ($_SESSION["user_flag"] == 1) {
            $management = "管理";
        } ?>
        <h3>ログイン<?= $management ?>アカウント名：<?= $_SESSION["account"] ?></h3>
    <?php } ?>