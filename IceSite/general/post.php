<?php
require_once '../common/set.php';
require_once '../common/list.php';

$title = '投稿';

if (!empty($_POST['convinience_flag'])) {
    $convinienceFlag = htmlspecialchars($_POST['convinience_flag']);
} else if (!empty($_POST['shop_flag'])) {
    $shopFlag = htmlspecialchars($_POST['shop_flag']);
}


require_once '../common/header.php';
require_once '../template/general/post.tpl.php';
require_once '../common/footer.php';
