<?php
require_once '../common/set.php';
require_once '../common/sql.php';
require_once '../common/list.php';

//投稿取得
$result = getIceUser($dbh);

//ユーザー市販アイス投稿取得
$convenience = getUserConveniencePost($dbh);

//ユーザーお店投稿取得
$shop = getUserShopPost($dbh);


$title = 'ユーザー';

require_once '../common/header.php';
require_once '../template/general/userHome.tpl.php';
require_once '../common/footer.php';
