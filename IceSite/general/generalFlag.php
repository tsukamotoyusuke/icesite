<?php
require_once '../common/set.php';
require_once '../common/sql.php';

if (!empty($_POST['post_flag'])) {

    //市販アイス
    if ($_POST['post_flag'] == 1) {
        $postTitle = htmlspecialchars($_POST['post_title']);
        $iceName = htmlspecialchars($_POST['ice_name']);
        $price = htmlspecialchars($_POST['price']);
        $maker = htmlspecialchars($_POST['maker']);
        $taste = htmlspecialchars($_POST['taste']);
        $recommend = htmlspecialchars($_POST['recommend']);
        $picture = htmlspecialchars($_POST['picture']);
        $review = htmlspecialchars($_POST['review']);

        $result = convenienceIceInsert($dbh, $postTitle, $iceName, $price, $maker, $taste, $recommend, $picture, $review);

        header('Location:userHome.php');
        exit;

        //お店
    } else if ($_POST['post_flag'] == 2) {
        $postTitle = htmlspecialchars($_POST['post_title']);
        $shopName = htmlspecialchars($_POST['shop_name']);
        $price = htmlspecialchars($_POST['price']);
        $location = htmlspecialchars($_POST['location']);
        $station = htmlspecialchars($_POST['station']);
        $distance = htmlspecialchars($_POST['distance']);
        $recommend = htmlspecialchars($_POST['recommend']);
        $picture = htmlspecialchars($_POST['picture']);
        $parking = htmlspecialchars($_POST['parking']);
        $review = htmlspecialchars($_POST['review']);

        $result = shopIceInsert($dbh, $postTitle, $shopName, $price, $location, $station, $distance, $recommend, $picture, $parking, $review);

        header('Location:userHome.php');
        exit;
    }
}
