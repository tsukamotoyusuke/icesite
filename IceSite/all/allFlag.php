<?php
require_once '../common/set.php';
require_once '../common/sql.php';

//ユーザー登録
if (!empty($_POST['register_flag'])) {
    $id = htmlspecialchars($_POST['id']);
    $account = htmlspecialchars($_POST['account']);
    $password = htmlspecialchars($_POST['password']);
    $mail = htmlspecialchars($_POST['mail']);
    $userFlag = 0;

    $hpass = password_hash($password, PASSWORD_DEFAULT);

    $result = iceUserInsert($dbh, $id, $account, $hpass, $mail, $userFlag);
    header('Location:home.php');
    exit;
}

//ログイン
if (!empty($_POST['login_flag'])) {
    $id = htmlspecialchars($_POST['id']);
    $password = htmlspecialchars($_POST['password']);

    list($result, $resultAccount, $resultUserFlag) = generalLogin($dbh, $id, $password);

    if ($result == false) {
        header('Location:login.php');
        exit;
    } else if ($result == true) {
        session_regenerate_id(TRUE); //セッションidを再発行
        $_SESSION["id"] = $id; //セッションにログイン情報を登録
        $_SESSION["account"] = $resultAccount;
        $_SESSION["user_flag"] = $resultUserFlag;
        header('Location:../general/userHome.php');
        exit;
    }
}
