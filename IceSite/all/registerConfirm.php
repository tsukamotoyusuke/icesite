<?php
require_once '../common/set.php';

$title = 'ユーザー登録確認';

$id = htmlspecialchars($_POST['id']);
$account = htmlspecialchars($_POST['account']);
$password = htmlspecialchars($_POST['password']);
$passwordConfirm = htmlspecialchars($_POST['passwordConfirm']);
$mail = htmlspecialchars($_POST['mail']);

require_once '../common/header.php';
require_once '../template/all/registerConfirm.tpl.php';
require_once '../common/footer.php';
