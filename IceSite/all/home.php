<?php
require_once '../common/set.php';
require_once '../common/sql.php';
require_once '../common/list.php';

//市販アイス投稿取得
$convenience = getConvenienceIce($dbh);

//お店投稿取得
$shop = getShopIce($dbh);


$title = 'ホーム';

require_once '../common/header.php';
require_once '../template/all/home.tpl.php';
require_once '../common/footer.php';
